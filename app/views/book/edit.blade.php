@extends('layout')

@section('header')
	Edit {{{$book->title}}}
@stop

@section('leftmenu')

@section('content')

@if($errors->has())
	<ul>
		@foreach ($errors->all() as $error)
	 	<li>{{ $error }}</li>
		@endforeach
	</ul>
@endif

{{Form::model($book, array('route' => array('book.update', $book->id), 'method' => 'put'))}}

	<p>ID: {{Form::label('id', $book->id)}}</p>
	
	<p>Title: {{Form::text('title')}}</p>
	
	<p>ISBN: {{Form::text('isbn')}}</p>
	
	<p>Author: tomorrow!!!</p>

	<p>{{Form::submit('Update')}}</p>
	
{{Form::close()}}
@stop