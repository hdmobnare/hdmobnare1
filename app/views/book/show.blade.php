@extends('layout')

@section('header')
	{{{$book->title}}} [<a href="{{{URL::to('book')}}}/{{{$book->id}}}/edit">Edit</a>]
@stop

@section('leftmenu')

@section('content')

	{{{ isset($message) ? $message : '' }}}

	<p>ISBN: {{{$book->isbn}}}</p>
	<p>Published: {{{$book->publish_date}}}</p>   <em>not great - we will look later at formatting dates without putting PHP function calls into the view</em>
	<p>In Stock: {{{$book->in_stock}}}</p>
	
	<p>Author: {{{$author->name}}}</p>

	<p>All books by the author:</p>
	
	<ul>
	@foreach($allbooks as $abook)	
		<li>{{{$abook->title}}}</li>
	@endforeach
	</ul>
@stop
