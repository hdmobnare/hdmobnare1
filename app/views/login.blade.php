@extends('layout')

@section('header')
	Please Login
@stop

@section('content')
	{{Form::open(array('url' => 'login'))}}
		<!-- List any login errors -->
		@if($errors->has())
			<ul>
				@foreach ($errors->all() as $error)
			 	<li>{{ $error }}</li>
				@endforeach
			</ul>
		@endif

		<p>
			{{Form::label('username', 'Username')}}
			{{Form::text('username', '')}}
		</p>

		<p>
			{{Form::label('password', 'Password')}}
			{{Form::password('password')}}
		</p>

		<p>{{Form::submit('Login') }}</p>
	{{Form::close()}}
@stop