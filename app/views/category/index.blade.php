@extends('layout')

@section('header')
Category
@stop

@section('leftmenu')
@parent
@stop

@section('content')

@for ($i = 0; $i < count($categories); $i++)
    	<p><a href="{{{URL::to('category')}}}/{{{$categories[$i]->id}}}">{{$categories[$i]->category_name}}</a></p>
	@endfor
@stop