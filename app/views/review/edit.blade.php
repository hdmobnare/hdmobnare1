@extends('layout')

@section('header')
	Review {{{Book::find($id)}}}
@stop

@section('leftmenu')

@section('content')

@if($errors->has())
	<ul>
		@foreach ($errors->all() as $error)
	 	<li>{{ $error }}</li>
		@endforeach
	</ul>
@endif

{{Form::model($abook, array('route' => array('review.update', $abook->id), 'method' => 'put'))}}
	
	<p>Rating: {{Form::selectRange('Review', 1,5)}}</p>
	
	<p>ID: {{Form::label('id', $book->id)}}</p>
	
	<p>Title: {{Form::text('title')}}</p>
	<p>Comments: {{Form::textarea('comments')}}</p>
	

	<p>{{Form::submit('Submit Review')}}</p>
	
{{Form::close()}}
@stop