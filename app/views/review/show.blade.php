@extends('layout')

@section('header')
	
@stop

@section('leftmenu')

@section('content')

@foreach($allbooks as $abook)
{{{$abook->title}}} [<a href="{{{URL::to('review')}}}/{{{$abook->id}}}/edit">Review</a>]


	<p>ISBN: {{{$abook->isbn}}}</p>
	<p>Published: {{{$abook->publish_date}}}</p> 
	<p>In Stock: {{{$abook->in_stock}}}</p>

	@endforeach
	</ul>
@stop