<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
this is a test commit to see if it uploads to git
|
*/

//App::bind('AuthorService', 'larkin\service\impl\AuthorServiceImpl');

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('nowtime', 'TimeController@now');

//Route::get('books', 'BookController@listAll');

Route::get('firstbook', 'BookController@getFirst');

Route::get('authors', 'AuthorController@listAuthors') ->before('auth');



//Route::post('book', 'BookController@add');
/*
Route::get('books', function()
{
	$books = Book::all();

	//$book = Book::where('isbn', '345353535')->get();
	foreach($book as $b) {
		echo $b->title . ' (' . $author->name . ') <br/>';
	}
	
	return View::make('booklist')->with('books', $books);
});
*/
//Route::get('authors', array('before' => 'auth'), 'AuthorController@listAuthors');
Route::get('authors', 'AuthorController@listAuthors')->before('auth');
//Route::get('categories','BookController@showCategories');
Route::group(array('prefix' => LaravelLocalization::setLocale()), function()
{
	Route::resource('book', 'BookController');
});

//for displaying our category and all the books contained
//Route::resource('category','CategoryController');

// Authentication/authorisation routes
Route::get('login', 'SecurityController@showLogin');
Route::post('login', 'SecurityController@doLogin');
Route::get('logout', 'SecurityController@doLogout');
Route::get('signup', 'SecurityController@doSignup') ->before('auth');
Route::post('update', 'SecurityController@update');
Route::get('librarian', 'SecurityController@makeLibrarian');
Route::get('member', 'SecurityController@makeMember');
Route::get('admin', 'SecurityController@makeAdmin');
Route::get('librarian', 'LibrarianController@displayMembers');
Route::post('memberdetails', 'LibrarianController@displayMemberDetails');
Route::resource('category', 'CategoryController');
//Route::get('category', 'CategoryController@index');
Route::get('processLoan', 'LibrarianController@processLoans');
//Route::resource('book', 'BookController');
Route::post('loanUpdate', 'LibrarianController@loanUpdate');

Route::resource('review', 'ReviewController');


