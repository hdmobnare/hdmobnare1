<?php
use larkin\service\AuthorService;
class AuthorController extends BaseController {
	private $authorService;
	function __construct(AuthorService $authorService) {
		$this->authorService = $authorService;
	}
	public function listAuthors() {
		$authors = $this->authorService->getAll();
		return View::make('listAuthors')->with('authors', $authors);
	}
	public function viewAuthor() { // must get a parameter here...
		return View::make('viewAuthor')->with('authors',
				$this->authorService->getById(1));
	}
	
	public function listIrishAuthors() {
		$authors = $this->authorService->getIrish();
		return View::make('listAuthors')->with('authors', $authors);
	}

}