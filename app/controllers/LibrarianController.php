<?php
class LibrarianController extends BaseController {
	public function displayMembers() {
		$display = DB::table ( 'users' )->where ( 'role', null )->get ();
		// $temp = $display->where('role', null)->get();
		// $temp=$display
		/*
		 * $temp; foreach ($display as $member){ if($display->role==null){ } }
		 */
		return View::make ( 'librarian' )->with ( 'memberslist', $display );
	}
	public function displayMemberDetails() {
		$display = DB::table ( 'users' )->whereNull ( 'role' )->update ( array (
				'role' => 'member' 
		) );
		$display1 = DB::table ( 'users' )->where ( 'role', null )->get ();
		return View::make ( 'librarian' )->with ( 'memberslist', $display1 );
	}
	public function processLoans() {
		return View::make ( 'loans' );
	}
	public function loanUpdate() {
		
		$uid = DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->pluck ( 'uniqueid' );
		$arr = explode ( ",", $uid );
		
		if (Input::has ( 'loan' )) {
			$loan = new Loan ();
			$loan->userid = Input::get ( 'userid' );
			$loan->bookid = Input::get ( 'titles' );
			$loan->uniqueid = Input::get ( 'uniqueid' );
			// $loan->bookid = Book::get()->lists('title', 'id', '')
			$loan->updated_at = null;
			$loan->save ();
			
			$arr = explode ( ",", $uid );
			$resultarr;
			foreach ( $arr as $unid ) {
				if (Input::get ( 'uniqueid' ) != $unid)
					$resultarr [] = $unid;
			}
			$result = implode ( ",", $resultarr );
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->update ( array (
					'uniqueid' => $result 
			) );
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->decrement ( 'in_stock' );
			DB::table ( 'users' )->where ( 'id', Input::get ( 'userid' ) )->decrement ( 'Book_allowance' );
		} 

		else{
			array_push($arr, Input::get ( 'uniqueid' ));
			$result = implode ( ",", $arr );
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->update ( array (
			'uniqueid' => $result
			) );
			
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->increment ( 'in_stock' );
			DB::table ( 'users' )->where ( 'id', Input::get ( 'userid' ) )->increment ( 'Book_allowance' );
		}
			
			return Redirect::to ( 'librarian' );
	}
}