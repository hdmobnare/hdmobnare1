<?php

class BookController extends \BaseController {

	function __construct() {
		$this->beforeFilter('auth', array('except' =>
			array('index', 'show')));
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$books = Book::all();
		
		return View::make('book.index')->with('books', $books);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$book = Book::find($id);
		$author = $book->author;
		$allbooks = $author->books;
		
		$message = Session::get('message', '');
		
		return View::make('book.show')->with('book', $book)->with('author', $author)
			->with('allbooks', $allbooks)->with('message', $message);
	}
/**	public function showCategories(){
 		$book = Book::all();
 		$categories = null;
 		foreach ($book as $b){
 		$category = $b->category;
 		
 		$categories = array_merge($categories, $category->books);
 		}
 		return View::make('category.show')->with('category', $categories);
	}
	
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$book = Book::find($id);
		return View::make('book.edit')->with('book', $book);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$inputs = Input::all();
		$validator = Book::validate($inputs);
		
		if ($validator->passes()) {
			$book = Book::find($id);	
			
			$book->title = $inputs['title'];
			$book->isbn = $inputs['isbn'];
			$book->update();
		
			return Redirect::route('book.show', $id)->with('message', 'Book updated.');
		} else {
			return Redirect::route('book.edit', $id)->withErrors($validator);
		}
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}