<?php

class SecurityController extends BaseController {

	public function showLogin()
	{
		return View::make('login');
	}
	
	public function doLogin()
	{
		
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
		$un = Input::get('username');
		$role = DB::table('users')->where('username',$un)->pluck('role');
		if (Auth::attempt($userdata)) {
			if($role=='admin')
				return Redirect::to('admin');
			else if($role=='librarian'){
				return Redirect::to('librarian');
			}
			else if($role=='member')
				return Redirect::to('member');
			else
				return Redirect::to('login');
		} else {
			return View::make('signup');
		}
	}
	
	public function doLogout()
	{
		Auth::logout(); 
		return Redirect::to('login'); 
	}

	
	public function doSignup()
	{
		return View::make('signup');
	}
	
	public function update()
	{
		$date = new DateTime;
		$user = new User;
		$user->username = Input::get('username');
		$user->password = Hash::make (Input::get('password'));
		$user->email = Input::get('email');
		$user->created_at = $date;
		$user->updated_at = $date;
		$user->name = Input::get('name');
		$user->address = Input::get('address');
		$user->phone = Input::get('phone');
//		DB::table('users')->insert(array($inputs));
/* 		DB::insert('insert into users (username, password, email, created_at, 
				updated_at, name, address, phone) values (?, ?, ?,$date,$date, ?, ?, ?)', array($inputs));	 */
		$user->save();
		return Redirect::to('book');
		} 
		
		public function makeLibrarian(){
			return View::make('librarian');
		}

		public function makeMember(){
			return View::make('member');
		}
		
		
		public function makeAdmin(){
			return View::make('admin');
		}
}