<?php

class ReviewController extends \BaseController {

// 		function __construct() {
// 		$this->beforeFilter('auth', array('except' =>
// 				array('index', 'show')));
// 	}

	public function index()
	{
		$books = Book::all();

		return View::make('review.index')->with('books', $books);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$allbooks = Book::all();

		return View::make('review.show')->with('allbooks', $allbooks);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('review.edit')->with($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function review($id)
	{
		$book = Book::find($id);
		return View::make('review.edit')->with('book', $book);
	
	}

}