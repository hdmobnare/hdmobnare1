<?php
namespace larkin\repository\impl;

use larkin\repository\AuthorRepository;
use larkin\mapper\AuthorRowMapper;
use Illuminate\Support\Facades\DB;

class DbAuthorRepository implements AuthorRepository {

	private $rowMapper;
	
	function __construct() {
		$this->rowMapper = new AuthorRowMapper();
	}
	
	function getById($id) {
		$author = DB::table('authors')->where('id', $id);
		return $this->rowMApper->mapRow($author);
	}
	
	function getAll() {
		$authors = DB::table('authors')->get();
		$rowMapper = new AuthorRowMapper();
		return $this->rowMapper->mapRowToArray($authors);
	}
}