<?php
namespace larkin\mapper;

use larkin\Author;

class AuthorRowMapper implements RowMapper {
	
	function mapRow($row) {
		
		$author = new Author();
		$author->setId($row->id);
		$author->setName($row->name);
		
		return $author;
	}
	
	function mapRowToArray($rows) {
		
		$authors = array();
		
		foreach($rows as $row) {
			$author = $this->mapRow($row);
			$authors[] = $author;
		}
		
		return $authors;
	}
}