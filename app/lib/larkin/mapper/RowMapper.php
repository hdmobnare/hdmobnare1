<?php
namespace larkin\mapper;

interface RowMapper {
	public function mapRow($row);
}