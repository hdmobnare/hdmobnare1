<?php
namespace larkin\service\impl;

use larkin\service\LoanService;
use larkin\repository\LoanRepository;

class LoanServiceImpl implements LoanService {

	function getById($id) ;
	
	function getAll();
}