<?php

use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create ( 'reviews', function ($table) {
			$table->increments ( 'id' );
			$table->string ( 'comment' )->nullable ();
			$table->integer ( 'rating' )->nullable ();
			$table->integer ( 'book_id' );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reviews');
	}

}
